#!/usr/bin/env ruby

require 'loled/version'

class Loled
  def initialize(filename, input=$stdin)
    @@input = input

    @filename = filename
    @modified = false
    @lines = [nil]
    @current_line = 1

    if File.exist?(filename)
      @lines += open(filename, 'r').read.split("\n", -1)
    else
      $stderr.puts "#{filename}: No such file or directory"
    end
  end

  def get_and_run(input=@@input) # TODO: This seems more than a bit gross.
    print '> '
    line = input.gets

    return nil if line.nil?

    line = line.chomp
    addresses, command, argument = parse(line)

    puts line unless @@input == $stdin

    if line == '.'
      puts @lines[@current_line]
    elsif command.nil? && !addresses.nil?
      @current_line = addresses.last
      puts @lines[@current_line]
    elsif respond_to?(command)
      m = method(command)

      args = []
      args << addresses || current_address if m.arity > 0
      args << argument if m.arity == 2

      instance_exec(*args, &method(command))

      @last_command = command.to_s
    else
      $stderr.puts "?"
    end

    true
  end

  require 'loled/commands'

private
  def read_chunk(input=@@input)
    text = ''
    line = nil

    loop do
      line = input.gets

      break if line == ".\n"

      text += line.to_s
    end

    text.split("\n", -1)
  end

  def current_address
    [@current_line, @current_line]
  end

  def parse(text)
    raw_addresses, command, argument = text.match(/^([^a-zA-Z]+)?([a-zA-Z]+)?(.*)$/).captures
    addresses = parse_range(raw_addresses)
    command = command.to_sym unless command.nil?

    [addresses, command, argument]
  end

  def parse_range_section(section)
    case section
    when '.'
      @current_line
    when '$'
      @lines.length - 1
    when '+'
      @current_line + 1
    when '-', '^'
      @current_line - 1
    when /[+-]\d+/
      @current_line + Integer(section)
    else
      Integer(section)
    end
  end

  def parse_range(addresses)
    return nil unless addresses

    addresses = '1,$' if addresses[0] == ',' || addresses.include?(',,')  || addresses.include?('%')
    addresses = ".,$" if addresses.include?(';')

    range = addresses.split(',').map(&method(:parse_range_section))
    range = [range.first, range.first] if range.length == 1

    range
  end
end
