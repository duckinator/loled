require 'rbsed'

class Loled
  # Insert
  def i((first, last))
    @modified = true

    @lines[first..first] = [*@lines[first], *read_chunk]
  end

  # Append
  def a((first, last))
    @modified = true

    @lines[last..last] = [@lines[last], *read_chunk]
  end

  #def c do
    # Replace addressed lines.
  #end

  # Delete
  def d((first, last))
    @modified = true

    @lines[first..last] = []
  end

  #def m((first, last), destination)
    # Move
  #end

  def n((first, last))
    # Print lines, with line numbers.

    puts @lines[first..last].each_with_index.map {|line, index|
      "#{(index + 1).to_s.ljust(8)}#{line}"
    }
  end

  def p((first, last))
    # Print lines
    puts @lines[first..last]
  end

  def s((first, last), expression)
    # Text replace
#    original = @lines[first..last].join("\n")
#    new = original.sed("s" + expression)
#
#    @modified = original != new
#
#    @lines[first..last] = new.split("\n", -1)
    lines =
      @lines[first..last].map do |original|
        new = original.sed("s" + expression)
  
        @modified = @modified || original != new
  
        new
      end

    @lines[first..last] = lines
  end

  #def t((first, last), destination)
    # I forget what this does.
  #end

  def w
    open(@filename, 'w') do |f|
      f.puts @lines.drop(1).join("\n")
    end

    @modified = false
  end

  def q
    if @modified && @last_command != 'q'
      puts '?'
    else
      exit
    end
  end
end
