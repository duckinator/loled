#!/usr/bin/env ruby

$: << File.join(File.dirname(__FILE__), 'lib')
require 'loled'

require 'stringio'
input = StringIO.new(<<-EOF)
i
hello, world!
line 2!
.
1a
really line 2!
.
$i
meep!
hello!
mrrrrrrrrrr
beep boop
beeeeeeep
.
3s/2/3/
2
.
,p
4,5d
,n
w
q
EOF

editor = Loled.new('test.txt', input)
while editor.get_and_run
  #...
end


#editor.commands[:i].call("hello, world!")
#editor.commands[:a].call("line 2!")
